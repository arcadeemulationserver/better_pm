# textdomain: betterpm


### init.lua ###

You don't have the privilege to send private messages=
You can't send messages to @1=
@1 is not online=
Write private messages=
<name>=
<message>=
Alias for /msg=
You must write to someone before replying. Use /msg <name> <message>=
Reply to a private message=
You are no longer ignoring all private messages=
You are now ignoring all private messages=
You are no longer ignoring private messages from @1=
You are now ignoring private messages from @1=
Ignore private messages from specific players or from everyone (/pmignore)=
